#!/usr/bin/env sh

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.0/Hack.zip 
unzip Hack.zip -d Hack
mkdir -p ~/.local/share/fonts
mv Hack ~/.local/share/fonts
rm Hack.zip
