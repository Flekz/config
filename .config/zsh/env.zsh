export EDITOR="hx"
export VISUAL="hx"
export SUDO_EDITOR="hx"
export PAGER="bat"

export HISTFILE="$ZDOTDIR/.zhistory"    # History filepath
export HISTSIZE=10000                   # Maximum events for internal history
export SAVEHIST=10000                   # Maximum events in history file
