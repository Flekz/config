#!/usr/bin/env bash

set -e

# Base Fedora Image
podman build base_container -t f38:base

# Default-cli container
distrobox create -i f38:base --name default-cli
distrobox-enter --name default-cli -- sudo dnf install -y zathura zathura-pdf-mupdf
distrobox-enter --name default-cli -- sudo dnf install -y cmake
# Export cli tools
# distrobox-enter --name default-cli -- kdir -p $HOME/.local/bin
# distrobox-enter --name default-cli -- distrobox-export -b $(which hx) -ep $HOME/.local/bin
distrobox-enter --name default-cli -- distrobox-export -b $(which fd) -ep $HOME/.local/bin
distrobox-enter --name default-cli -- distrobox-export -b $(which zathura) -ep $HOME/.local/bin
distrobox-enter --name default-cli -- distrobox-export -b $(which tldr) -ep $HOME/.local/bin

# Base Alpine Image
# podman build alpine_container -t alpine:base
