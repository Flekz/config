#!/usr/bin/env sh

set -e
distrobox create -c $1 tmp
podman rm $1
podman rename tmp $1