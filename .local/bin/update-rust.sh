#!/usr/bin/env bash
set -e

echo "Updating rustup and packages with cargo-update/binstall"
distrobox-enter default-cli -- /home/$USER/.cargo/bin/rustup update
distrobox-enter default-cli -- /home/$USER/.cargo/bin/cargo install-update -a