#!/usr/bin/env zsh

# Don't clutter up home folder
export XDG_CONFIG_HOME="$HOME/.config"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

if [[ -d "$HOME/.cargo" ]]; then
  . "$HOME/.cargo/env"
fi
